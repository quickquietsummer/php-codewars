<?php

namespace Katas\K02_02_2023;

use PHPUnit\Framework\TestCase;

function expanded_form(int $n): string
{
    preg_match_all('/[^0]/', $n, $matches, PREG_OFFSET_CAPTURE);
    $matches = array_map(function ($match) use ($n) {
        return str_pad($match[0], strlen($n) - $match[1], '0');
    }, $matches[0]);
    return implode(' + ', $matches);
}

class ExpandedFormTest extends TestCase
{
    public function testDescriptionExamples()
    {
//        $this->assertSame("10 + 2", expanded_form(12));
//        $this->assertSame("40 + 2", expanded_form(42));
        $this->assertSame("70000 + 300 + 4", expanded_form(70304));
    }
}
