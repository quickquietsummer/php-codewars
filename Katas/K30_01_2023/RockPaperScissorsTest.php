<?php

namespace Katas\K30_01_2023;

use PHPUnit\Framework\TestCase;

interface ISign
{
    /**
     * @return array{string:int}
     */
    public function rules(): array;

    public function name(): string;
}

class SignComparer
{

    public function compare(ISign $signSelf, ISign $signAnother): int
    {
        $weightSelf = array_keys($signSelf->rules(), $signSelf::class);
        $weightAnother = array_keys($signSelf->rules(), $signAnother::class);

        if ($weightSelf > $weightAnother) {
            return 1;
        } elseif ($weightSelf < $weightAnother) {
            return -1;
        } else {
            return 0;
        }
    }
}

class Rock implements ISign
{
    public function __construct()
    {
        $this->signComparer = new SignComparer($this);
    }

    public function rules(): array
    {
        return [
            Scissors::class,
            Rock::class,
            Paper::class,
        ];
    }

    public function name(): string
    {
        return 'rock';
    }
}

class Scissors implements ISign
{

    public function rules(): array
    {
        return [
            Paper::class,
            Scissors::class,
            Rock::class,
        ];
    }

    public function name(): string
    {
        return 'scissors';
    }
}


class Paper implements ISign
{

    public function rules(): array
    {
        return [
            Rock::class,
            Paper::class,
            Scissors::class
        ];
    }

    public function name(): string
    {
        return 'paper';
    }
}

class SignPool
{
    /**
     * @return array<ISign>
     */
    public function evaluate(): array
    {
        return [
            new Rock(),
            new Scissors(),
            new Paper()
        ];
    }
}

class SignResolver
{
    public function __construct(private SignPool $signPool)
    {
    }

    public function resolve(string $s): ISign
    {
        foreach ($this->signPool->evaluate() as $sign) {
            if ($sign->name() === $s) {
                return $sign;
            }
        }

        throw new \InvalidArgumentException('НЕТ ТАКОГО ЗНАКА');
    }
}

function rpc($p1, $p2)
{
    $signPool = new SignPool();
    $signResolver = new SignResolver($signPool);
    $signComparer = new SignComparer();

    $p1 = $signResolver->resolve($p1);
    $p2 = $signResolver->resolve($p2);
    $result = $signComparer->compare($p1, $p2);

    $draw = 'Draw!';
    $win1 = 'Player 1 won!';
    $win2 = 'Player 2 won!';

    return match ($result) {
        1 => $win1,
        -1 => $win2,
        0 => $draw
    };
}

class RockPaperScissorsTest extends TestCase
{
    private function dotest($p1, $p2, $exp)
    {
        $actual = rpc($p1, $p2);
        //echo $exp == $actual;
        $this->assertSame($exp, $actual);
    }

    public function testrpcBasics()
    {
        $this->dotest("rock", "scissors", "Player 1 won!");
        $this->dotest("scissors", "rock", "Player 2 won!");
        $this->dotest("scissors", "scissors", "Draw!");
    }
}
