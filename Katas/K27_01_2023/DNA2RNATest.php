<?php

namespace Katas\K27_01_2023;

use PHPUnit\Framework\TestCase;

/**
 * Deoxyribonucleic acid, DNA is the primary information storage molecule in biological systems. It is composed of four nucleic acid bases Guanine ('G'), Cytosine ('C'), Adenine ('A'), and Thymine ('T').
 *
 * Ribonucleic acid, RNA, is the primary messenger molecule in cells. RNA differs slightly from DNA its chemical structure and contains no Thymine. In RNA Thymine is replaced by another nucleic acid Uracil ('U').
 *
 * Create a function which translates a given DNA string into RNA.
 *
 * For example:
 *
 * "GCAT"  =>  "GCAU"
 * The input string can be of arbitrary length - in particular, it may be empty. All input is guaranteed to be valid, i.e. each input string will only ever consist of 'G', 'C', 'A' and/or 'T'.
 */
function dnaToRna($str)
{
    return str_replace('T', 'U', $str);
}

class DNA2RNATest extends TestCase
{
    public function testFixedTests()
    {
        $this->assertSame('UUUU', dnaToRna("TTTT"));
        $this->assertSame('GCAU', dnaToRna("GCAT"));
        $this->assertSame("", dnaToRna(""));
        $this->assertSame('U', dnaToRna("T"));
        $this->assertSame('GACCGCCGCC', dnaToRna("GACCGCCGCC"));
        $this->assertSame('GAUUCCACCGACUUCCCAAGUACCGGAAGCGCGACCAACUCGCACAGC', dnaToRna("GATTCCACCGACTTCCCAAGTACCGGAAGCGCGACCAACTCGCACAGC"));
        $this->assertSame('CACGACAUACGGAGCAGCGCACGGUUAGUACAGCUGUCGGUGAACUCCAUGACA', dnaToRna("CACGACATACGGAGCAGCGCACGGTTAGTACAGCTGTCGGTGAACTCCATGACA"));
        $this->assertSame('CACGACAUACGGAGCAGCGCACGGUUAGUACAGCUGUCGGUGAACUCCAUGACA', dnaToRna("CACGACATACGGAGCAGCGCACGGTTAGTACAGCTGTCGGTGAACTCCATGACA"));
        $this->assertSame('AACCCUGUCCACCAGUAACGUAGGCCGACGGGAAAAAUAAACGAUCUGUCAAUG', dnaToRna("AACCCTGTCCACCAGTAACGTAGGCCGACGGGAAAAATAAACGATCTGTCAATG"));
    }
}
