<?php

namespace Katas\K20_01_2023;

/**
 * Digital root is the recursive sum of all the digits in a number.
 *
 * Given n, take the sum of the digits of n. If that value has more than one digit, continue reducing in this way until a single-digit number is produced. The input will be a non-negative integer.
 *
 * Examples
 * 16  -->  1 + 6 = 7
 * 942  -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
 * 132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
 * 493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2
 */
function digital_root($number): int
{
    $digits = array_map('intval', str_split($number));
    $sum = 0;
    foreach ($digits as $digit) {
        $sum += (int)$digit;
    }
    if ($sum > 9) {
        return digital_root($sum);
    }
    return $sum;
}


class DigitalRootTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider basicTestDataProvider
     */
    public function testBasic($input, $expected)
    {
        $actual = digital_root($input);
        $this->assertSame($expected, $actual, 'Digital root calculation failed!');
    }

    public function basicTestDataProvider()
    {
        return [
            [16, 7],
            [195, 6],
            [992, 2],
            [999999999999, 9],
            [167346, 9],
            [10, 1],
            [0, 0],
        ];
    }
}

