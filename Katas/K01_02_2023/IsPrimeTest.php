<?php
namespace Katas\K01_02_2023;

use PHPUnit\Framework\TestCase;

function is_prime(int $n): bool {

}

final class IsPrimeTest extends TestCase {
    public function testExamples() {
        $this->assertFalse(is_prime(0));
        $this->assertFalse(is_prime(1));
        $this->assertTrue(is_prime(2));
        $this->assertTrue(is_prime(5), 'Your function should work for the example provided in the Kata Description');
    }
}
