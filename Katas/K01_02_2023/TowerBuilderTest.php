<?php

namespace Katas\K01_02_2023;

use PHPUnit\Framework\TestCase;

function tower_builder(int $n): array
{
    $wide = 1;
    $widest = ($n * 2) - 1;
    $result = [str_pad('*', $widest, ' ', STR_PAD_BOTH)];

    for ($i = 1; $i < $n; $i++) {
        $wide += 2;
        $row = str_pad('*', $wide, '*');
        $row = str_pad($row, $widest, ' ', STR_PAD_BOTH);
        $result[] = $row;
    }

    return $result;
}

class TowerBuilderTest extends TestCase
{
    public function testBaseCase()
    {
        $this->assertSame(['*'], tower_builder(1));
    }

    public function testDescriptionExamples()
    {
        $this->assertSame([
            '  *  ',
            ' *** ',
            '*****'
        ], tower_builder(3));
        $this->assertSame([
            '     *     ',
            '    ***    ',
            '   *****   ',
            '  *******  ',
            ' ********* ',
            '***********'
        ], tower_builder(6));
    }
}
