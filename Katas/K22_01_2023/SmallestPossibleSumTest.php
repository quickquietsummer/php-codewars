<?php

namespace Katas\K22_01_2023;
use PHPUnit\Framework\TestCase;

/**Description
 * Given an array X of positive integers, its elements are to be transformed by running the following operation on them as many times as required:
 *
 * if X[i] > X[j] then X[i] = X[i] - X[j]
 *
 * When no more transformations are possible, return its sum ("smallest possible sum").
 *
 * For instance, the successive transformation of the elements of input X = [6, 9, 21] is detailed below:
 *
 * X_1 = [6, 9, 12] # -> X_1[2] = X[2] - X[1] = 21 - 9
 * X_2 = [6, 9, 6]  # -> X_2[2] = X_1[2] - X_1[0] = 12 - 6
 * X_3 = [6, 3, 6]  # -> X_3[1] = X_2[1] - X_2[0] = 9 - 6
 * X_4 = [6, 3, 3]  # -> X_4[2] = X_3[2] - X_3[1] = 6 - 3
 * X_5 = [3, 3, 3]  # -> X_5[1] = X_4[0] - X_4[1] = 6 - 3
 * The returning output is the sum of the final transformation (here 9).
 *
 * Example
 * solution([6, 9, 21]) #-> 9
 * Solution steps:
 * [6, 9, 12] #-> X[2] = 21 - 9
 * [6, 9, 6] #-> X[2] = 12 - 6
 * [6, 3, 6] #-> X[1] = 9 - 6
 * [6, 3, 3] #-> X[2] = 6 - 3
 * [3, 3, 3] #-> X[1] = 6 - 3
 * Additional notes:
 * There are performance tests consisted of very big numbers and arrays of size at least 30000. Please write an efficient algorithm to prevent timeout.
 */
function solution(array $numbers): int
{
    $count = count($numbers);
    if ($count === 1) {
        return $numbers[0];
    }
    $numbersTemp = $numbers;

    $forGcd = [];

    $min1 = min($numbersTemp);
    $minKey1 = array_keys($numbersTemp, $min1)[0];
    $forGcd[] = $min1;
    array_splice($numbersTemp, $minKey1, 1);

    $min2 = min($numbersTemp);
    $minKey2 = array_keys($numbers, $min2)[0];
    $forGcd[] = $min2;
    array_splice($numbersTemp, $minKey2, 1);

    if ($count > 2) {
        $max1 = max($numbersTemp);
        $maxKey1 = array_keys($numbersTemp, $max1)[0];
        $forGcd[] = $max1;
        array_splice($numbersTemp, $maxKey1, 1);
    }
    if ($count > 3) {
        $max2 = max($numbersTemp);
        $maxKey2 = array_keys($numbersTemp, $max2)[0];
        $forGcd[] = $max2;
        array_splice($numbersTemp, $maxKey2, 1);
    }

    $gcd = array_reduce($forGcd, 'K22_01_2023\gcd', $forGcd[0]);

    echo("forGcd is:\n");
    print_r($forGcd);
    echo "\n";
    printf("min1 is: %d\n", $min1);
    printf("min2 is: %d\n", $min2);
    printf("max1 is: %d\n", $max1 ?? 'none');
    printf("max2 is: %d\n", $max2 ?? 'none');
    printf("gcd is: %d\n", $gcd);
    print_r($numbers);

    if ($gcd === 1) {
        return $count;
    }

    foreach ($numbersTemp as $key => $number) {
        $remains = $number % $gcd;

        if ($remains !== 0) {
            printf("Число по ключу %d со значением %d не делится без остатка на %d. Остаток %d", $key, $number, $gcd, $remains);
            return gcd($gcd, $number)*$count;
        }

    }

    return $gcd * $count;
}

function gcd($a, $b)
{
    while ($a !== $b)
        if ($a > $b)
            $a -= $b;
        else
            $b -= $a;
    return $a;
}

class SmallestPossibleSumTest extends TestCase
{
    public function testExample()
    {
        $this->assertSame(9, solution([6, 9, 21]));
    }
}
