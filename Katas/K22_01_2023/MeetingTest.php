<?php

namespace Katas\K22_01_2023;

use PHPUnit\Framework\TestCase;

/**John has invited some friends. His list is:
 *
 * s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";
 * Could you make a program that
 *
 * makes this string uppercase
 * gives it sorted in alphabetical order by last name.
 * When the last names are the same, sort them by first name. Last name and first name of a guest come in the result between parentheses separated by a comma.
 *
 * So the result of function meeting(s) will be:
 *
 * "(CORWILL, ALFRED)(CORWILL, FRED)(CORWILL, RAPHAEL)(CORWILL, WILFRED)(TORNBULL, BARNEY)(TORNBULL, BETTY)(TORNBULL, BJON)"
 * It can happen that in two distinct families with the same family name two people have the same first name too.
 *
 * Notes
 * You can see another examples in the "Sample tests".
 */
function meeting($s)
{
    $namesRaw = explode(';', $s);
    $namesPrepared = array_map(function ($name) {
        $name = strtoupper($name);
        preg_match('/(.*):(.*)/', $name, $matches);
        return sprintf('(%s, %s)', $matches[2], $matches[1]);
    }, $namesRaw);
    sort($namesPrepared, SORT_STRING);

    return implode('', $namesPrepared);
}

class MeetingTest extends TestCase
{

    public function dotest($s, $expect)
    {
        printf("s: %s\r\n", $s);
        $actual = meeting($s);
        printf("Actual: %s\r\n", $actual);
        printf("Expect: %s\r\n", $expect);
        $this->assertSame($expect, $actual);
        printf("%s\r\n", "-");
    }

    public function testBasics()
    {
        $this->dotest("Alexis:Wahl;John:Bell;Victoria:Schwarz;Abba:Dorny;Grace:Meta;Ann:Arno;Madison:STAN;Alex:Cornwell;Lewis:KERN;Megan:Stan;Alex:Korn",
            "(ARNO, ANN)(BELL, JOHN)(CORNWELL, ALEX)(DORNY, ABBA)(KERN, LEWIS)(KORN, ALEX)(META, GRACE)(SCHWARZ, VICTORIA)(STAN, MADISON)(STAN, MEGAN)(WAHL, ALEXIS)");
        $this->dotest("John:Gates;Michael:Wahl;Megan:Bell;Paul:Dorries;James:Dorny;Lewis:Steve;Alex:Meta;Elizabeth:Russel;Anna:Korn;Ann:Kern;Amber:Cornwell",
            "(BELL, MEGAN)(CORNWELL, AMBER)(DORNY, JAMES)(DORRIES, PAUL)(GATES, JOHN)(KERN, ANN)(KORN, ANNA)(META, ALEX)(RUSSEL, ELIZABETH)(STEVE, LEWIS)(WAHL, MICHAEL)");

    }
}
